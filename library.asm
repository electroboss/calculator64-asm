section .text
numparse:
  ; finds numerical value for base-10
  ; string at r8, with length rcx (can fit in cl)
  ; and store in rbx
  
  mov rbx,0
  mov r9,10
  ; for every digit, starting with most significant, times by ten rcx times and add to rbx
.digitloop:
  mov rax,0
  mov al,[r8]
  sub al,'0'

  push rcx
.powerloop:
  dec rcx
  jz .powerend
  mul r9 ; multiply rax (the digit) by r9 (10)
  jmp .powerloop

.powerend:
  add rbx,rax

  inc r8
  pop rcx
  loop .digitloop

  ret


findnumstrlen:
  ; finds length of string at rax,
  ; with rcx being the string length 
  add rcx,rax
.findlenstart:
  dec rcx
  cmp byte [rcx],'0'
  jge .findlenend
  cmp rcx,rax
  jne .findlenstart
.findlenend:
  sub rcx,rax
  inc rcx
  ret

numtostr:
; takes rax:rbx and outputs to string at rcx
  mov r9,rcx
  mov r10,10
.digitloop:
  mov rdx,0
  div r10
  mov r8,rax
  mov rax,rbx
  div r10
  mov rbx,rax
  mov rax,r8
  add dl,'0'
  mov [rcx],dl
  dec rcx
  
  mov r11,rax
  or r11,rbx
  jnz .digitloop

.end:
  inc rcx
  ret
