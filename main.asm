%include "library.asm"

section .text
global _start
_start:
  mov eax,4
  mov ebx,1
  mov ecx,firstnum_prompt
  mov edx,firstnum_prompt_len
  int 80h

  mov eax,3
  mov ebx,2
  mov ecx,firstnumstr
  mov edx,21 ; twenty digits max (up to 2^64-1=18446744073709551615), plus newline
  int 80h

  mov eax,4
  mov ebx,1
  mov ecx,secondnum_prompt
  mov edx,secondnum_prompt_len
  int 80h

  mov eax,3
  mov ebx,2
  mov ecx,secondnumstr
  mov edx,21 ; twenty digits max (up to 2^64-1=18446744073709551615), plus newline
  int 80h

  ; find length
  mov rax,firstnumstr
  mov rcx,numstr_len
  call findnumstrlen

  ; parse
; mov rcx,rcx
  mov r8,firstnumstr
  call numparse ; parse number
  mov [firstnum],rbx

  ; same again for secondnumstr
  mov rax,secondnumstr
  mov rcx,numstr_len
  call findnumstrlen
; mov rcx,rcx
  mov r8,secondnumstr
  call numparse
  mov [secondnum],rbx

  mov eax,4
  mov ebx,1
  mov ecx,operator_prompt
  mov edx,operator_prompt_len
  int 80h

  mov eax,3
  mov ebx,2
  mov ecx,operator
  mov edx,1
  int 80h

  mov ebx,0
  mov ecx,0
  mov al,[operator]
  mov rbx,[firstnum]
  mov rcx,[secondnum]
  cmp al,"+"
  je .addition
  cmp al,"-"
  je .subtraction
  cmp al,"*"
  je .multiplication
  cmp al,"/"
  je .division
  jmp .invalid

.addition:
  xor rax,rax
  add rbx,rcx
  jnc .print
  inc rax
  jmp .print
.subtraction:
  sub rbx,rcx
  mov rax,0
  jmp .print
.multiplication:
  mov rax,rbx
  mul rcx
  mov rbx,rax
  mov rax,rdx
  jmp .print
.division:
  mov rax,rbx
  mov rdx,0
  div rcx

  mov r12,rdx
  mov r9,rax

  mov eax,4
  mov ebx,1
  mov ecx,quotient
  mov edx,quotient_len
  int 80h

  mov rbx,r9
  xor eax,eax
  mov rcx,outputstring+outputstring_len
  call numtostr

  mov eax,4
  mov ebx,1
; mov ecx,ecx
; mov edx,edx
  int 80h

  mov eax,4
  mov ebx,1
  mov ecx,remainder
  mov edx,remainder_len
  int 80h

  mov rbx,r12
  xor eax,eax
  mov rcx,outputstring+outputstring_len
  call numtostr

  mov eax,4
  mov ebx,1
; mov ecx,ecx
; mov edx,edx
  int 80h

  mov eax,4
  mov ebx,1
  mov ecx,newline
  mov edx,1
  int 80h

  jmp .end
.invalid:
  mov eax,4
  mov ebx,1
  mov ecx,invalid_char
  mov edx,invalid_char_len
  int 80h
  jmp .end

.print:
  mov rcx,outputstring+outputstring_len-1
  call numtostr

  mov eax,4
  mov ebx,1
; mov edx,edx
; mov ecx,ecx
  int 80h
  mov eax,4
  mov ebx,1
  mov ecx,newline
  mov edx,1
  int 80h

  ; jmp .end

.end:
  mov eax,1
  mov ebx,0
  int 80h


section .data
  firstnum_prompt db "Enter first number",0Ah
  firstnum_prompt_len equ $ - firstnum_prompt
  secondnum_prompt db "Enter second number",0Ah
  secondnum_prompt_len equ $ - secondnum_prompt
  operator_prompt db "Enter operator",0Ah
  operator_prompt_len equ $ - operator_prompt
  invalid_char db "Invalid char. Quitting.",0Ah
  invalid_char_len equ $ - invalid_char
  newline db 0Ah
  quotient db "Quotient: "
  quotient_len equ $ - quotient
  remainder db 0Ah,"Remainder: "
  remainder_len equ $ - remainder
section .bss
  firstnumstr resb 20 ; for max value of 2^64-1=18446744073709551615
  secondnumstr resb 20 ; ``
  numstr_len equ 20
  firstnum resq 1
  secondnum resq 1
  operator resb 1 ; +-*/
  outputstring resb 39 ; for max value of 18446744073709551615*18446744073709551615
                       ; = 340282366920938463426481119284349108225 (bloody hell that's
                       ; long)
  outputstring_len equ $ - outputstring