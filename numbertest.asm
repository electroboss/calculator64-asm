%include "library.asm"

section .text
global _start
_start:
  mov rax,0xFFFFFFFFFFFFFFFE
  mov rbx,0x0000000000000001
  mov rcx,test_string_bss+99
  call numtostr

  mov eax,4
  mov ebx,1
  mov rcx,test_string_bss
  mov edx,100
  int 80h

  mov eax,1
  mov ebx,0
  int 80h

section .data
  test_string db "1850820284397780626"
  test_string_len equ $ - test_string

section .bss
  test_string_bss resb 100